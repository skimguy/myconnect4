//
//  ViewController.swift
//  connect4
//
//  Created by Tyler Mulley
//  Copyright © 2017 Tyler Mulley. All rights reserved.
//
//  This file is used for testing 

import UIKit

class ViewController: UIViewController {
    
    var board = Array(repeating: Array(repeating: Checker(color: Color.blank), count: Constants.COLUMNS), count: Constants.ROWS)
    let winChecker = WinChecker()
    
    var player1 = HumanPlayer()
    var player2 = AIPlayer()
    
    var players = Array<Player>()
    var currentPlayerIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // set up the game
        setupGame()
        
        // start the game
        runGameLoop()
    }
    
    func printTurn(numTurns: Int) {
        print(numTurns)
        printBoard()
    }
    
    func printBoard() {
        //print("rows \(board.count) cols: \(board[0].count)")
        for row in board {
            print(row)
        }
    }
    
    func putCheckerAt(col: Int, color: Color) {
        for row in (0...board.count-1).reversed() {
            // if the current checker is blank
            if board[row][col].color == Color.blank {
                board[row][col].color = color
                return
            } else if row-1 > -1 {
                // if the current checker is blank and the one below it is red or yellow
                if board[row][col].color != Color.blank && board[row-1][col].color == Color.blank {
                    board[row-1][col].color = color
                    return
                }
            }
        }
    }
    
    func runGameLoop() {
        var numTurns = 0
        
        while (true) {
            printTurn(numTurns: numTurns)
            
            // get current player
            let currentPlayer = players[currentPlayerIndex]
            
            // get next move from the current player
            let col = currentPlayer.ask()
            
            // check if the move is valid
            if (isMoveValid(col: col)) {
                // make the move
                putCheckerAt(col: col, color: currentPlayer.getColor())
                numTurns += 1
            } else {
                print("Invalid Move. Game Over.")
                printTurn(numTurns: numTurns)
                return
            }
            
            // switch turns to the next player
            nextPlayer()
            
            // tell the other player about the move
            players[currentPlayerIndex].tell(board: board)
            
            // if there is a winner, break out of the loop
            if (winChecker.isWinner(board: board)) {
                do {
                    if let winner = try winChecker.getWinner() {
                        switch winner {
                        case Color.red:
                            print("Red won.")
                            break
                        case Color.yellow:
                            print("Yellow won.")
                            break
                        default: // including Color.blank
                            print("Tie.")
                            break
                        }
                        printTurn(numTurns: numTurns)
                        return
                    }
                } catch {
                    print("Tie.")
                    printTurn(numTurns: numTurns)
                    return
                }
            }
        }
    }
    
    func setupGame() {
        // before the first turn tell both players the board
        players.append(player1)
        players.append(player2)
        
        for player in players {
            player.tell(board: board)
        }
    }
    
    func isMoveValid(col: Int) -> Bool {
        // if the column is in the range
        if col > -1 && col < Constants.COLUMNS && !isColumnFull(col: 0) {
            return true
        } else {
            return false
        }
    }
    
    func isColumnFull(col: Int) -> Bool {
        if board[0][col].color == Color.blank {
            return false
        } else {
            return true
        }
    }
    
    func nextPlayer() {
        currentPlayerIndex = (currentPlayerIndex + 1) % players.count
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

