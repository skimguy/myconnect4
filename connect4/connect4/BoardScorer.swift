//
//  BoardScorer.swift
//  connect4
//
//  Created by Tyler Mulley
//  Copyright © 2017 Tyler Mulley. All rights reserved.
//
//  This file makes a class BoardScorer that the AI uses to score
//  the current board and decide on its next move


import Foundation

class BoardScorer {
    
    let winChecker = WinChecker()
    let inARowChecker = InARowChecker()
    
    // Returns the current score of the board passed
    func score(board: Array<Array<Checker>>, color: Color) -> Int {
        
        var score = 0
        var opponentColor = Color.blank
        if color == .red {
            opponentColor = .yellow
        } else {
            opponentColor = .red
        }
        
        // Try to win
        if winChecker.isWinner(board: board) {
            do {
                if let winner = try winChecker.getWinner() {
                    if (winner == color) {
                        score += 1000
                    } else {
                        score += -1000
                    }
                }
            } catch {
                score += 0
            }
        }
        
        // Defend against win
        if (inARowChecker.threeInARow(board: board, color: color) > 0) {
            score += 20
        }
        
        if (inARowChecker.threeInARow(board: board, color: opponentColor) > 0) {
            score += -20
        }
        
        if (inARowChecker.twoInARow(board: board, color: color) > 0) {
            score += 5
        }
        
        if (inARowChecker.twoInARow(board: board, color: opponentColor) > 0) {
            score += -5
        }
        
        score += countCheckerScore(board: board, color: color)
        
        //print("SCORE: \(score)")
        if color == .yellow {
            score = -score
        }
        return score
    }
    
    // Returns the score based on how many checkers of a certain
    // color are on the board
    func countCheckerScore(board: Array<Array<Checker>>, color: Color) -> Int {
        var score = 0
        for i in 0...board.count-1 {
            for j in 0...board[i].count-1 {
                if board[i][j].color == color {
                    switch j {
                    case 0:
                        score += 0
                        break
                    case 1:
                        score += 1
                        break
                    case 2:
                        score += 2
                        break
                    case 3:
                        score += 3
                        break
                    case 4:
                        score += 2
                        break
                    case 5:
                        score += 1
                        break
                    case 6:
                        score += 0
                        break
                    default:
                        score += 0
                    }
                }
            }
        }
        
        return score
    }
    
    // Prints the board to the console
    func printBoard(board: Array<Array<Checker>>) {
        //print("rows \(board.count) cols: \(board[0].count)")
        for row in board {
            print(row)
        }
    }
}
