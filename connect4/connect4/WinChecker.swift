//
//  WinChecker.swift
//  connect4
//
//  Created by Tyler Mulley
//  Copyright © 2017 Tyler Mulley. All rights reserved.
//
//  This file makes a class WinChecker that checks to see if a player has won the game
//  by taking a gameboard and testing it.

import Foundation

class WinChecker {
    
    internal var winningColor: Color? = nil
    
    // Checks all possible ways a player could win
    // Returns true for win and false if no winner is found
    func isWinner(board: Array<Array<Checker>>) -> Bool {
        if checkHorizontal(color: .red, board: board, num: 4) || checkVertical(color: .red, board: board, num: 4) || checkDiagonal(color: .red, board: board, num: 4) {
            winningColor = Color.red
            return true
        } else if checkHorizontal(color: .yellow, board: board, num: 4) || checkVertical(color: .yellow, board: board, num: 4) || checkDiagonal(color: .yellow, board: board, num: 4) {
            winningColor = Color.yellow
            return true
        } else if isBoardFull(board: board) {
            winningColor = Color.blank
            return true
        }
        else {
            return false
        }
    }
    
    // Returns the color of the winning player
    func getWinner() throws -> Color? {
        if winningColor != nil {
            return winningColor!
        } else {
            throw GameError.NoWinner
        }
    }
    
    // Checks if board is filled
    // Returns true if so and false if not
    internal func isBoardFull(board: Array<Array<Checker>>) -> Bool {
        for row in board {
            for cell in row {
                if cell.color == Color.blank {
                    return false
                }
            }
        }
        return true
    }
    
    // Checks if a player has won in the horizontal direction
    // Returns true if a winner is found and false if not
    internal func checkHorizontal(color: Color, board: Array<Array<Checker>>, num: Int) -> Bool {
        var checkersInARow = 0
        for r in 0...board.count-1 {
            for c in 0...board[0].count-1 {
                if  checkersInARow > num-1 { break }
                if board[r][c].color == color {
                    checkersInARow += 1
                    if checkersInARow >= num {
                        return true
                    }
                } else {
                    checkersInARow = 0
                }
            }
            if checkersInARow < num {
                checkersInARow = 0
            } else {
                //print("HORIZONTAL: \(color)")
                return true
            }
        }
        
        return false
    }
    
    // Checks if a player has won in the vertical direction
    // Returns true if a winner is found and false if not
    internal func checkVertical(color: Color, board: Array<Array<Checker>>, num: Int) -> Bool {
        var checkersInARow = 0
        for c in 0...board[0].count-1 {
            for r in 0...board.count-1 {
                if board[r][c].color == color {
                    checkersInARow += 1
                    if checkersInARow >= num {
                        return true
                    }
                } else {
                    checkersInARow = 0
                }
            }
            if checkersInARow < num {
                checkersInARow = 0
            } else {
                //print("VERTICAL: \(color)")
                return true
            }
        }
        return false
    }
    
    // Checks if a player has won in the diagonal direction
    // Returns true if a winner is found and false if not
    internal func checkDiagonal(color: Color, board: Array<Array<Checker>>, num: Int) -> Bool {
        var checkersInARow = 0
        for r in 0...board.count-1 {
            for c in 0...board[r].count-1 {
                if (board[r][c].color == color) {
                    checkersInARow += 1
                    for i in 1...num-1 {
                        if checkersInARow > num-1 { break }
                        if (r+i < board.count && c+i < board[0].count && board[r+i][c+i].color == color) {
                            checkersInARow += 1
                            if checkersInARow >= num {
                                return true
                            }
                        } else {
                            checkersInARow = 1
                        }
                    }
                    
                    if checkersInARow < num {
                        checkersInARow = 1
                    } else {
                        return true
                    }
                    
                    for i in 1...num-1 {
                        if checkersInARow > num-1 { break }
                        if (r-i > -1 && c-i > -1 && board[r-i][c-i].color == color) {
                            checkersInARow += 1
                            if checkersInARow >= num {
                                return true
                            }
                        } else {
                            checkersInARow = 1
                        }
                    }
                    
                    if checkersInARow < num {
                        checkersInARow = 1
                    } else {
                        return true
                    }
                    
                    for i in 1...num-1 {
                        if checkersInARow > num-1 { break }
                        if (r-i > -1 && c+i < board[0].count && board[r-i][c+i].color == color) {
                            checkersInARow += 1
                            if checkersInARow >= num {
                                return true
                            }
                        } else {
                            checkersInARow = 1
                        }
                    }
                    
                    if checkersInARow < num {
                        checkersInARow = 1
                    } else {
                        return true
                    }
                    
                    for i in 1...num-1 {
                        if checkersInARow > num-1 { break }
                        if (r+i < board.count && c-i > -1 && board[r+i][c-i].color == color) {
                            checkersInARow += 1
                            if checkersInARow >= num {
                                return true
                            }
                        } else {
                            checkersInARow = 1
                        }
                    }
                    
                    if checkersInARow < num {
                        checkersInARow = 1
                    } else {
                        return true
                    }
                }
                checkersInARow = 0
            }
        }
        return false
    }
    
}
