//
//  Checker.swift
//  connect4
//
//  Created by Tyler Mulley
//  Copyright © 2017 Tyler Mulley. All rights reserved.
//

import Foundation
import UIKit

struct Checker: CustomStringConvertible {
    
    var color = Color.blank
    var player = 0
    
    init(color: Color) {
        self.color = color
    }
    
    init(player: Int, color: Color) {
        self.player = player
        self.color = color
    }
    
    var description: String {
        if color == Color.red {
            return "R"
        } else if color == Color.yellow {
            return "Y"
        } else {
            return "0"
        }
    }
}
