//
//  InARowChecker.swift
//  connect4
//
//  Created by Tyler Mulley
//  Copyright © 2017 Tyler Mulley. All rights reserved.
//
//  This file makes a class InARowChecker that
//  checks for certain rows of checkers on the board

import Foundation

class InARowChecker {
    
    func threeInARow(board: Array<Array<Checker>>, color: Color) -> Int{
        return checkHorizontal(color: color, board: board, num: 3) + checkVertical(color: color, board: board, num: 3) + checkDiagonal(color: color, board: board, num: 3)
    }
    
    func twoInARow(board: Array<Array<Checker>>, color: Color) -> Int {
        return checkHorizontal(color: color, board: board, num: 2) + checkVertical(color: color, board: board, num: 2) + checkDiagonal(color: color, board: board, num: 2)
    }
    
    func inMiddle(board: Array<Array<Checker>>, color: Color) -> Int {
        return checkInMiddle(color: color, board: board)
    }
    
    func isValidMove(board: Array<Array<Checker>>, move: Int) -> Bool {
        for i in 0...board.count-1 {
            if board[i][move].color == .blank {
                return true
            }
        }
        return false
    }
    
    internal func checkInMiddle(color: Color, board: Array<Array<Checker>>) -> Int {
        var score = 0
        for i in 0...board.count-1 {
            if board[i][3].color == color {
                score += 1
            }
        }
        return score
    }
    
    internal func checkHorizontal(color: Color, board: Array<Array<Checker>>, num: Int) -> Int {
        var checkersInARow = 0
        for r in 0...board.count-1 {
            for c in 0...board[0].count-1 {
                if  checkersInARow > num-1 { break }
                if board[r][c].color == color {
                    checkersInARow += 1
                    if checkersInARow >= num {
                        if ((c+1 < board[r].count && board[r][c+1].color == Color.blank) && (c-num > -1 && board[r][c-num].color == Color.blank)) {
                            return num*10
                        } else if (c+1 < board[r].count && board[r][c+1].color == Color.blank) {
                            return num
                        } else if (c-num > -1 && board[r][c-num].color == Color.blank) {
                            return num
                        } else {
                            checkersInARow = 0
                        }
                    }
                } else {
                    checkersInARow = 0
                }
            }
            if checkersInARow < num {
                checkersInARow = 0
            } else {
                //print("HORIZONTAL: \(color)")
                return num
            }
        }
        
        return 0
    }
    
    internal func checkVertical(color: Color, board: Array<Array<Checker>>, num: Int) -> Int {
        var checkersInARow = 0
        for c in 0...board[0].count-1 {
            for r in 0...board.count-1 {
                if board[r][c].color == color {
                    checkersInARow += 1
                    if checkersInARow >= num {
                        if ((r+1 < board.count && board[r+1][c].color == Color.blank) && (r-num > -1 && board[r-num][c].color == Color.blank)) {
                            return num*10
                        } else if (r+1 < board.count && board[r+1][c].color == Color.blank) {
                            return num
                        } else if (r-num > -1 && board[r-num][c].color == Color.blank) {
                            return num
                        } else {
                            checkersInARow = 0
                        }
                    }
                } else {
                    checkersInARow = 0
                }
            }
            if checkersInARow < num {
                checkersInARow = 0
            } else {
                //print("VERTICAL: \(color)")
                return num
            }
        }
        return 0
    }
    
    internal func checkDiagonal(color: Color, board: Array<Array<Checker>>, num: Int) -> Int {
        var checkersInARow = 0
        for r in 0...board.count-1 {
            for c in 0...board[r].count-1 {
                if (board[r][c].color == color) {
                    checkersInARow += 1
                    if checkersInARow >= num {
                        return num
                    }
                    for i in 1...num-1 {
                        if checkersInARow > num-1 { break }
                        if (r+i < board.count && c+i < board[0].count && board[r+i][c+i].color == color) {
                            checkersInARow += 1
                            if checkersInARow >= num {
                                if ((r+1 < board.count && c+1 < board[r].count && board[r+1][c+1].color == Color.blank) && (r-num > -1 && c-num > -1 && board[r-num][c-num].color == Color.blank)) {
                                    return num*10
                                } else if (r+1 < board.count && c+1 < board[r].count && board[r+1][c+1].color == Color.blank) {
                                    return num
                                } else if (r-num > -1 && c-num > -1 && board[r-num][c-num].color == Color.blank) {
                                    return num
                                } else {
                                    checkersInARow = 0
                                }
                            }
                        } else {
                            checkersInARow = 1
                        }
                    }
                    
                    if checkersInARow < num {
                        checkersInARow = 1
                    } else {
                        return num
                    }
                    
                    for i in 1...num-1 {
                        if checkersInARow > num-1 { break }
                        if (r-i > -1 && c-i > -1 && board[r-i][c-i].color == color) {
                            checkersInARow += 1
                            if checkersInARow >= num {
                                if ((r-1 > -1 && c-1 > -1 && board[r-1][c-1].color == Color.blank) && (r+num < board.count && c+num < board[0].count && board[r+num][c+num].color == Color.blank)) {
                                    return num*10
                                } else if (r-1 > -1 && c-1 > -1 && board[r-1][c-1].color == Color.blank) {
                                    return num
                                } else if (r+num < board.count && c+num < board[0].count && board[r+num][c+num].color == Color.blank) {
                                    return num
                                } else {
                                    checkersInARow = 0
                                }
                            }
                        } else {
                            checkersInARow = 1
                        }
                    }
                    
                    if checkersInARow < num {
                        checkersInARow = 1
                    } else {
                        return num
                    }
                    
                    for i in 1...num-1 {
                        if checkersInARow > num-1 { break }
                        if (r-i > -1 && c+i < board[0].count && board[r-i][c+i].color == color) {
                            checkersInARow += 1
                            if checkersInARow >= num {
                                if ((r-1 > -1 && c+1 < board[0].count && board[r-1][c+1].color == Color.blank) && (r+num < board.count && c-num > -1 && board[r+num][c-num].color == Color.blank)) {
                                    return num*10
                                } else if (r-1 > -1 && c+1 < board[0].count && board[r-1][c+1].color == Color.blank) {
                                    return num
                                } else if (r+num < board.count && c-num > -1 && board[r+num][c-num].color == Color.blank) {
                                    return num
                                } else {
                                    checkersInARow = 0
                                }
                            }
                        } else {
                            checkersInARow = 1
                        }
                    }
                    
                    if checkersInARow < num {
                        checkersInARow = 1
                    } else {
                        return num
                    }
                    
                    for i in 1...num-1 {
                        if checkersInARow > num-1 { break }
                        if (r+i < board.count && c-i > -1 && board[r+i][c-i].color == color) {
                            checkersInARow += 1
                            if checkersInARow >= num {
                                if ((r+1 < board.count && c-1 > -1 && board[r+1][c-1].color == Color.blank) && (r-num > -1 && c+num < board[0].count && board[r-num][c+num].color == Color.blank)) {
                                    return num*10
                                } else if (r+1 < board.count && c-1 > -1 && board[r+1][c-1].color == Color.blank) {
                                    return num
                                } else if (r-num > -1 && c+num < board[0].count && board[r-num][c+num].color == Color.blank) {
                                    return num
                                } else {
                                    checkersInARow = 0
                                }
                            }
                        } else {
                            checkersInARow = 1
                        }
                    }
                    
                    if checkersInARow < num {
                        checkersInARow = 1
                    } else {
                        return num
                    }
                }
                checkersInARow = 0
            }
        }
        return 0
    }
    
}
