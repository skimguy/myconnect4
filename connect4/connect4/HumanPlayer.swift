//
//  AIPlayer.swift
//  connect4
//
//  Created by Tyler Mulley
//  Copyright © 2017 Tyler Mulley. All rights reserved.
//
//  This file makes a class HumanPlayer that inherits from player
//  this is the user

import Foundation

class HumanPlayer: Player {
    
    override func getColor() -> Color {
        return Color.yellow
    }
    
    // ask the player what colum to drop the checker in
    override func ask() -> Int {
        return Int(arc4random_uniform(7))
    }

}
