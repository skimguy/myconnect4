//
//  PossibleMovesChecker.swift
//  connect4
//
//  Created by Tyler Mulley
//  Copyright © 2017 Tyler Mulley. All rights reserved.
//
//  This file makes a class PossibleMovesChecker that 
//  returns a list of all the possible moves during the game

import Foundation

class PossibleMovesChecker {
    
    // Returns a list of possible moves given a specific gameboard
    func getPossibleMoves(board: Array<Array<Checker>>) -> [Int] {
        var possibleMoves = [Int]()
        for i in 0...board[0].count-1 {
            if board[0][i].color == .blank {
                possibleMoves.append(i)
            }
        }
        return possibleMoves
    }
}
