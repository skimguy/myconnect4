//
//  GameViewController.swift
//  connect4
//
//  Created by Tyler Mulley
//  Copyright © 2017 Tyler Mulley. All rights reserved.
//
//  This file creates the game board on the view 
//  and starts and continues the flow of the game

import UIKit

class GameViewController: UIViewController {
    
    let squareSideLength = (UIScreen.main.bounds.width - 20) / 7
    let halfSide = (UIScreen.main.bounds.width - 20) / 14
    let startingY = 100.0
    var game = GameScene()
    var human = HumanPlayer()
    var checkerViews = [UIView]()
    var gameOver = false
    var gameOverView = UIView()
    var gameOverLabel = UILabel()
    var difficulty:Int = 5
    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        human = game.humanPlayer
        game.gameView = self
        game.aiPlayer.maxDepth = difficulty
        
        setupBoard()
        
        displayGameOverView(winner: Winner.none)
    }
    
    // Restarts the game when the restart button is tapped
    func resetGame() {
        for checkerView in checkerViews {
            checkerView.removeFromSuperview()
        }
        game = GameScene()
        game.aiPlayer.maxDepth = difficulty
        human = game.humanPlayer
        game.gameView = self
        gameOver = false
        gameOverLabel.text = ""
        setupBoard()
    }
    
    // Setups the initial game board
    func setupBoard() {
        var squares : [Square] = []
        view.backgroundColor = UIColor.groupTableViewBackground
        
        // make board
        var currentCenter = CGPoint(x: Double(halfSide + 10.0), y: startingY)
        for i in 1...7 {
            for _ in 1...6 {
                let newSquare = getSquare()
                newSquare.view.center = currentCenter
                newSquare.view.restorationIdentifier = String(i)
                squares.append(newSquare)
                view.addSubview(newSquare.view)
                currentCenter.y += squareSideLength
            }
            currentCenter.x += squareSideLength
            currentCenter.y = CGFloat(startingY)
        }
    }
    
    // handles the tapping on the columns of the game board
    func handleTap(recognizer: UIGestureRecognizer) {
        if gameOver {
            return
        }
        let column : Int = Int((recognizer.view?.restorationIdentifier)!)!
        //print(column)

        let goodMove = game.checkMoveAndWin(col: column - 1, currentPlayer: human)
        if goodMove {
            makeMove(column: column - 1, player: human)
        }
    }
    
    // makes a move on the screen depending on player and column selected
    func makeMove(column: Int, player: Player) {
        let square = CGRect(x: 0, y: 0, width: squareSideLength - 1, height: squareSideLength - 1)
        let checkerView = UIView(frame: square)
        let xPos : Double = Double(halfSide + 10.0) + Double(squareSideLength * CGFloat(column))
        let yPos : Double = startingY + (Double(squareSideLength) * Double(game.rowToPlay))
        checkerView.center = CGPoint(x: xPos, y: -100)
        checkerView.layer.cornerRadius = checkerView.bounds.size.width / 2
        if player is HumanPlayer {
            checkerView.backgroundColor = UIColor.yellow
        }
        else {
            checkerView.backgroundColor = UIColor.red
        }
        checkerView.layer.zPosition = -1
        self.view.addSubview(checkerView)
        checkerViews.append(checkerView)
        
        UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseInOut, animations: {
            checkerView.center = CGPoint(x: Double(checkerView.center.x), y: yPos)
        }, completion: { finished in
            if player is HumanPlayer {
                self.game.runAITurn()
            }
        })
        
    }
    
    func displayGameOverView(winner: Winner) {
        gameOverView = UIView(frame: CGRect(x: self.view.frame.width/2.0 - (self.view.frame.width*0.75*0.5), y: self.view.frame.height/2.0 + (self.view.frame.height/12.0), width: self.view.frame.width*0.75, height: self.view.frame.height/6.0))
        gameOverView.backgroundColor = UIColor.gray
        
        gameOverLabel = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: gameOverView.frame.width, height: gameOverView.frame.height/2.0))
        
        gameOverLabel.center = CGPoint(x: gameOverView.bounds.size.width / 2.0, y: gameOverView.bounds.size.height/4.0)
        gameOverLabel.textAlignment = .center
        gameOverLabel.font = UIFont.boldSystemFont(ofSize: 30)
        gameOverLabel.textColor = UIColor.white
        
        let playAgainButton = UIButton(frame: CGRect(x: 5.0, y: gameOverView.bounds.size.height/2.0, width: gameOverView.frame.width - 10.0, height: gameOverView.frame.height/2.0 - 10.0))
        playAgainButton.backgroundColor = UIColor.blue
        playAgainButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 30)
        playAgainButton.titleLabel?.textColor = UIColor.white
        playAgainButton.setTitle("Reset Game", for: .normal)
        playAgainButton.layer.borderWidth = 3.0
        playAgainButton.layer.borderColor = UIColor.white.cgColor
        playAgainButton.layer.cornerRadius = 4.0
        playAgainButton.addTarget(self, action: #selector(playAgainButtonPressed), for: .touchUpInside)
        
        gameOverView.layer.cornerRadius = 4.0
        gameOverView.addSubview(gameOverLabel)
        gameOverView.addSubview(playAgainButton)
        
        
        if winner == Winner.user {
            gameOverLabel.text = "You Win!"
        } else if winner == Winner.ai {
            gameOverLabel.text = "You Lose."
        } else {
            gameOverLabel.text = ""
        }
        
        self.view.addSubview(gameOverView)
    }
    
    func playAgainButtonPressed() {
        resetGame()
    }
    
    // returns one of the squares used to build the game board
    func getSquare() -> Square {
        
        let square = CGRect(x: 0, y: 0, width: squareSideLength, height: squareSideLength)
        
        let whiteView = UIView(frame: square)
        let maskLayer = CAShapeLayer() //create the mask layer
        
        // Set the radius to 1/3 of the screen width
        let radius : CGFloat = (squareSideLength / 2) - 5
        // Create a path with the rectangle in it.
        let path = UIBezierPath(rect: whiteView.bounds)
        // Put a circle path in the middle
        
        path.addArc(withCenter: whiteView.center, radius: radius, startAngle: 0.0, endAngle: CGFloat(2*Double.pi), clockwise: true)
        
        // Give the mask layer the path you just draw
        maskLayer.path = path.cgPath
        // Fill rule set to exclude intersected paths
        maskLayer.fillRule = kCAFillRuleEvenOdd
        
        // By now the mask is a rectangle with a circle cut out of it. Set the mask to the view and clip.
        whiteView.layer.mask = maskLayer
        whiteView.clipsToBounds = true
        
        whiteView.transform = CGAffineTransform(scaleX: 1.01, y: 1.01)
        
        whiteView.backgroundColor = UIColor.blue
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(recognizer:)))
        let doubleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTap(recognizer:)))
        doubleTapGestureRecognizer.numberOfTapsRequired = 2
        tapGestureRecognizer.require(toFail: doubleTapGestureRecognizer)
        whiteView.addGestureRecognizer(doubleTapGestureRecognizer)
        whiteView.addGestureRecognizer(tapGestureRecognizer)
        
        let newSquare = Square(v: whiteView)
        
        return newSquare
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func handleDoubleTap(recognizer: UITapGestureRecognizer) {
        return
    }
    

}
