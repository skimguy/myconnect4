//
//  GameScene.swift
//  connect4
//
//  Created by Tyler Mulley
//  Copyright © 2017 Tyler Mulley. All rights reserved.
//
//  This file makes a class GameScene that creates an instance of a game


import UIKit

class GameScene {
    
    var board = Array(repeating: Array(repeating: Checker(color: Color.blank), count: Constants.COLUMNS), count: Constants.ROWS)
    let winChecker = WinChecker()
    var gameView : GameViewController!
    
    init() {
        // set up the game
        setupGame()
    }
    
    // Prints the board to the console
    func printBoard() {
        //print("rows \(board.count) cols: \(board[0].count)")
        for row in board {
            print(row)
        }
    }
    
    // puts a checker with the passed color at the passed column of the board
    // Returns the row in which the checker was played
    func putCheckerAt(col: Int, color: Color) -> Int {
        for row in (0...board.count-1).reversed() {
            // if the current checker is blank
            if board[row][col].color == Color.blank {
                board[row][col].color = color
                return row
            } else if row-1 > -1 {
                // if the current checker is blank and the one below it is red or yellow
                if board[row][col].color != Color.blank && board[row-1][col].color == Color.blank {
                    board[row-1][col].color = color
                    return row - 1
                }
            }
        }
        
        return 0
    }
    
    var aiPlayer = AIPlayer()
    //var aiPlayer = HumanPlayer()
    var humanPlayer = HumanPlayer()
    
    var players = Array<Player>()
    var currentPlayerIndex = 0
    var rowToPlay = 0
    
    // After the player plays, this function handles the AI Movement
    func runAITurn() {
        
        // switch turns to the next player
        //nextPlayer()
        
        if winChecker.isWinner(board: board) {
            gameOver(userWin: true)
            return
        }
        
        // get current player
        let currentPlayer = players[1]
        
        // get next move from the current player
        currentPlayer.tell(board: board)
        let col = currentPlayer.ask()
        
        // check if the move is valid
        let isGoodMove = checkMoveAndWin(col: col, currentPlayer: currentPlayer)
        
        if isGoodMove {
            gameView.makeMove(column: col, player: currentPlayer)
        }
        
    }
    
    // Checks to see if move is valid and if the move causes a win
    // returns false for invalid move and true for valid move
    func checkMoveAndWin(col: Int, currentPlayer: Player) -> Bool {
        if (isMoveValid(col: col)) {
            // make the move
            rowToPlay = putCheckerAt(col: col, color: currentPlayer.getColor())
        } else {
            print("Invalid Move.")
            gameOver(userWin: false)
            return false
        }
        
        // tell the other player about the move
        players[currentPlayerIndex].tell(board: board)
        
        // if there is a winner, break out of the loop
        if (winChecker.isWinner(board: board)) {
            //printBoard()
            do {
                if let winner = try winChecker.getWinner() {
                    printBoard()
                    switch winner {
                    case Color.red:
                        print("Red won.")
                        gameOver(userWin: false)
                        break
                    case Color.yellow:
                        print("Yellow won.")
                        gameOver(userWin: true)
                        break
                    default: // including Color.blank
                        print("Tie.")
                        gameOver(userWin: false)
                        break
                    }
                    return true
                }
            } catch {
                print("Tie.")
                gameOver(userWin: false)
                return true
            }
        }
        
        return isMoveValid(col: col)
    }
    
    // Sets up the players and the game
    func setupGame() {
        // before the first turn tell both players the board
        aiPlayer.tell(board: board)
        humanPlayer.tell(board: board)
        
        players.append(humanPlayer)
        players.append(aiPlayer)
    }
    
    // Checks to make sure the move made is a valid move
    // returns false for invalid move and true for valid move
    func isMoveValid(col: Int) -> Bool {
        if col > -1 && col < board[0].count {
            return true
        } else {
            return false
        }
    }
    
    // moves play to the next player
    func nextPlayer() {
        currentPlayerIndex = (currentPlayerIndex + 1) % players.count
    }
    
    // Runs when the game is over
    func gameOver(userWin: Bool) {
        print("Game Over")
        //printBoard()
        gameView.gameOver = true
        if userWin {
            gameView.displayGameOverView(winner: Winner.user)
        } else {
            gameView.displayGameOverView(winner: Winner.ai)
        }
        
    }
    
}

