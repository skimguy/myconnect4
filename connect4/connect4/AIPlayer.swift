//
//  AIPlayer.swift
//  connect4
//
//  Created by Tyler Mulley
//  Copyright © 2017 Tyler Mulley. All rights reserved.
//
//  This file makes a class AIPlayer that inherits from player
//  This is the computer player
//  This file also makes the Node class for the minimax tree
//  used in deciding the AI movements


import Foundation

class AIPlayer: Player {
    
    let winChecker = WinChecker()
    let possibleMovesChecker = PossibleMovesChecker()
    let boardScorer = BoardScorer()
    var maxDepth = 2
    
    override func getColor() -> Color {
        return Color.red
    }
    
    // Sets the AI's knowledge base to the current board state
    override func tell(board: Array<Array<Checker>>) {
        self.board = copyBoard(board: board)
    }
    
    // ask the player what colum to drop the checker in
    override func ask() -> Int {
        //return alphaBetaSearch(board: copyBoard(board: board))
        return search()
    }
    
    // Make a copy of the current board
    // Returns a copy of the board
    func copyBoard(board: Array<Array<Checker>>) -> Array<Array<Checker>>{
        var copyBoard = Array(repeating: Array(repeating: Checker(color: Color.blank), count: 7), count: 6)
        for i in 0...board.count-1 {
            for j in 0...board[i].count-1 {
                copyBoard[i][j].color = board[i][j].color
            }
        }
        
        return copyBoard
    }
    
    // Creates a copy of the board with a guessed move
    // returns copy of the board after the guess is made
    private func simulatePutCheckerAt(col: Int, color: Color, board: Array<Array<Checker>>) -> Array<Array<Checker>>? {
        var copyBoard = board
        for row in (0...board.count-1).reversed() {
            
            // if the current checker is blank
            if board[row][col].color == Color.blank {
                copyBoard[row][col].color = color
                return copyBoard
            } else if row-1 > -1 {
                
                // if the current checker is blank and the one below it is red or yellow
                if board[row][col].color != Color.blank && board[row-1][col].color == Color.blank {
                    copyBoard[row-1][col].color = color
                    return copyBoard
                }
            }
        }
        return nil
    }
    
    // returns a utility value for the game state given
    internal func utility(board: Array<Array<Checker>>, color: Color) -> Int {
        return boardScorer.score(board: board, color: color)
    }
    
    // Finds all the possible actions and returns the game state and the utility value
    internal func actions(board: Array<Array<Checker>>, color: Color) -> [Node] {
        var nodes = [Node]()
        let possibleMoves = possibleMovesChecker.getPossibleMoves(board: board)
        for m in possibleMoves {
            if let newBoard = simulatePutCheckerAt(col: m, color: color, board: board) {

                let score = utility(board: newBoard, color: color)
                nodes.append(Node(board: newBoard, move: m, score: score))
            }
        }
        return nodes
    }
    
    // Searches the tree for the move that gets the maximum score
    // Returns the best move for the AI to make
    func search() -> Int {
        var maxMoveScore = Int.min
        var maxMoveArray = [Int]()
        //printBoard(board: self.board)
        //print("MAX DEPTH: \(maxDepth)")
        for a in actions(board: copyBoard(board: self.board), color: .red) {
            //print("MOVE")
            //printBoard(board: a.board)
            let currentScore = alphaBetaSearch(board: a.board)
            //print(currentScore)
            if currentScore > maxMoveScore {
                //print("NEW MAX SCORE: \(currentScore)")
                //print("Move: \(a.move)")
                maxMoveScore = currentScore
                maxMoveArray = [Int]()
                maxMoveArray.append(a.move)
            } else if currentScore == maxMoveScore {
                maxMoveArray.append(a.move)
            }
        }
        
        if maxMoveArray.count == 1 {
            return maxMoveArray.first!
        } else {
            let random = Int(arc4random_uniform(UInt32(maxMoveArray.count)))
            return maxMoveArray[random]
        }
    }
    
    // Alpha-Beta-Search(state) returns an action
    internal func alphaBetaSearch(board: Array<Array<Checker>>) -> Int {
        //printBoard()
        let score = minValue(board: copyBoard(board: board), alpha: Int.min, beta: Int.max, depth: 0)
        //print("score: \(score)")
        return score
    }
    
    // Max-Value(state, a, b) returns a utility value
    internal func maxValue(board: Array<Array<Checker>>, alpha: Int, beta: Int, depth: Int) -> Int {
        if depth >= maxDepth {
            //print("Reached max depth: \(maxDepth)")
            return utility(board: board, color: .red)
        }
        var newAlpha = alpha
        let newBeta = beta
        
        if winChecker.isWinner(board: board) {
            return utility(board: board,color: .red)
        }
        
        var v = Int.min
        
        for a in actions(board: copyBoard(board: board), color: .red) {
            
            // v <- Max(v, Min-Value(Result(s,a), alpha, beta)
            v = max(v, minValue(board: a.board, alpha: newAlpha, beta: newBeta, depth: depth+1))
            
            // if v >= beta then return v
            if v >= newBeta {
                //print("Pruned at depth: \(depth)")
                return v
            }
            
            // alpha <- Max(alpha, v)
            newAlpha = max(newAlpha, v)
        }
        
        return v
    }
    
    // Min-Value(state, a, b) returns a utility value
    internal func minValue(board: Array<Array<Checker>>, alpha: Int, beta: Int, depth: Int) -> Int {
        if depth >= maxDepth {
            //print("Reached max depth: \(maxDepth)")
            return utility(board: board, color: .yellow)
        }
        let newAlpha = alpha
        var newBeta = beta
        
        if winChecker.isWinner(board: board) {
            return utility(board: board, color: .yellow)
        }
        
        var v = Int.max
        
        for a in actions(board: copyBoard(board: board), color: .yellow) {

            v = min(v, maxValue(board: a.board, alpha: newAlpha, beta: newBeta, depth: depth+1))
            if v <= newAlpha {
                //print("Pruned at depth: \(depth)")
                return v
            }
            
            newBeta = min(newBeta, v)
        }
        
        return v
    }
    
    func printBoard(board: Array<Array<Checker>>) {
        //print("rows \(board.count) cols: \(board[0].count)")
        for row in board {
            print(row)
        }
    }
}

class Node {
    var board: Array<Array<Checker>>
    var score: Int
    var move: Int

    init(board: Array<Array<Checker>>, move: Int, score: Int) {
        self.board = board
        self.move = move
        self.score = score
    }
}
