//
//  Player.swift
//  connect4
//
//  Created by Tyler Mulley
//  Copyright © 2017 Tyler Mulley. All rights reserved.
//
//  This file makes a class Player that creates a generic connect 4 player


import Foundation

class Player {
    internal var board = Array(repeating: Array(repeating: Checker(color: Color.blank), count: 7), count: 6)
    private var color = Color.blank
    
    func getColor() -> Color {
        return color
    }
    
    // update the player's view of the board
    func tell(board: Array<Array<Checker>>) {
        self.board = board
    }
    
    // ask the player what colum to drop the checker in
    func ask() -> Int {
        return 0
    }
    
}
