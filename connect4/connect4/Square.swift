//
//  Square.swift
//  connect4
//
//  Created by Tyler Mulley
//  Copyright © 2017 Tyler Mulley. All rights reserved.
//

import Foundation
import UIKit

class Square {
    var view : UIView
    
    init(v: UIView) {
        view = v
    }
}
