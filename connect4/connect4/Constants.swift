//
//  Constants.swift
//  connect4
//
//  Created by Tyler Mulley
//  Copyright © 2017 Tyler Mulley. All rights reserved.
//
//  This file makes a struct Constants for use in the game

import Foundation

enum Color { case red, yellow, blank }
enum Winner { case user, ai, none }
enum GameError: Error {
    case NoWinner
}

struct Constants {
    static let ROWS = 6
    static let COLUMNS = 7

}
