//
//  connect4Tests.swift
//  connect4
//
//  Created by Tyler Mulley
//  Copyright © 2017 Tyler Mulley. All rights reserved.
//
//  This file runs various unit tests for the project

import XCTest
@testable import connect4

class connect4Tests: XCTestCase {
    var vc:ViewController! = ViewController()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        vc = storyboard.instantiateViewController(withIdentifier: "main") as! ViewController
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testFullBoard() {
        let winChecker = WinChecker()
        
        for _ in 0...Constants.ROWS-1 {
            for j in 0...Constants.COLUMNS-1 {
                vc.putCheckerAt(col: j, color: .red)
            }
        }
        
        vc.printBoard()
        XCTAssertTrue(winChecker.isBoardFull(board: vc.board))
    }
    
    func testFullColumn() {
        let col = 0
        
        for _ in 0...Constants.ROWS-1 {
            vc.putCheckerAt(col: col, color: .red)
        }
        
        vc.printBoard()
        XCTAssertTrue(vc.isColumnFull(col: col))
    }
    
    func testInvalidMove() {
        for _ in 0...Constants.ROWS-1 {
            for j in 0...Constants.COLUMNS-1 {
                vc.putCheckerAt(col: j, color: .red)
            }
        }
        
        XCTAssertFalse(vc.isMoveValid(col: -1)) // Col out of range
        XCTAssertFalse(vc.isMoveValid(col: Constants.ROWS)) // Col out of range
        XCTAssertFalse(vc.isMoveValid(col: 0)) // Col full
    }
    
    func testWinCheckerHorizontalPositive() {
        let winChecker = WinChecker()
        
        vc.putCheckerAt(col: 0, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 1, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 2, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 3, color: Color.red)
        vc.printBoard()
        XCTAssertTrue(winChecker.isWinner(board: vc.board))
    }
    
    func testWinCheckerHorizontalPositive2() {
        let winChecker = WinChecker()
        
        vc.putCheckerAt(col: 0, color: Color.red)
        vc.putCheckerAt(col: 1, color: Color.yellow)
        vc.putCheckerAt(col: 2, color: Color.red)
        vc.putCheckerAt(col: 3, color: Color.yellow)
        
        vc.putCheckerAt(col: 0, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 1, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 2, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 3, color: Color.red)
        XCTAssertTrue(winChecker.isWinner(board: vc.board))
    }
    
    // Test col 0: red, yellow, 4 reds
    func testWinCheckerVerticalPositive3() {
        let winChecker = WinChecker()
        
        vc.putCheckerAt(col: 0, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        vc.putCheckerAt(col: 0, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        vc.putCheckerAt(col: 0, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        vc.putCheckerAt(col: 0, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        vc.putCheckerAt(col: 0, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        vc.putCheckerAt(col: 0, color: Color.red)
        XCTAssertTrue(winChecker.isWinner(board: vc.board))
    }
    
    func testWinCheckerHorizontalPositive3() {
        let winChecker = WinChecker()
        
        vc.putCheckerAt(col: 0, color: Color.red)
        vc.putCheckerAt(col: 1, color: Color.yellow)
        vc.putCheckerAt(col: 2, color: Color.red)
        vc.putCheckerAt(col: 3, color: Color.yellow)
        
        vc.putCheckerAt(col: 0, color: Color.yellow)
        vc.putCheckerAt(col: 1, color: Color.red)
        vc.putCheckerAt(col: 2, color: Color.yellow)
        vc.putCheckerAt(col: 3, color: Color.red)
        
        vc.putCheckerAt(col: 0, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 1, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 2, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 3, color: Color.red)
        XCTAssertTrue(winChecker.isWinner(board: vc.board))
    }
    
    func testWinCheckerHorizontalNegative4() {
        let winChecker = WinChecker()
        
        vc.putCheckerAt(col: 0, color: .red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 1, color: .yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 2, color: .red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 3, color: .yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 4, color: .yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 5, color: .yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 6, color: .red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
    }
    
    func testWinCheckerHorizontalPositive5() {
        let winChecker = WinChecker()
        
        vc.putCheckerAt(col: 0, color: .red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 1, color: .yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 2, color: .red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 3, color: .yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 4, color: .yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 5, color: .yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 6, color: .yellow)
        XCTAssertTrue(winChecker.isWinner(board: vc.board))
    }
    
    func testWinCheckerHorizontalPositive6() {
        let winChecker = WinChecker()
        
        vc.putCheckerAt(col: 0, color: .yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 1, color: .yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 2, color: .yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 3, color: .yellow)
        XCTAssertTrue(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 4, color: .red)
        XCTAssertTrue(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 5, color: .yellow)
        XCTAssertTrue(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 6, color: .red)
        XCTAssertTrue(winChecker.isWinner(board: vc.board))
    }
    
    func testWinCheckerHorizontalNegative() {
        let winChecker = WinChecker()
        
        vc.putCheckerAt(col: 0, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 1, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 2, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 3, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 3, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
    }
    
    func testWinCheckerVerticalPositive() {
        let winChecker = WinChecker()
        
        vc.putCheckerAt(col: 0, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 0, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 0, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 0, color: Color.red)
        XCTAssertTrue(winChecker.isWinner(board: vc.board))
    }
    
    func testWinCheckerVerticalPositive2() {
        let winChecker = WinChecker()
        
        vc.putCheckerAt(col: 0, color: .yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 0, color: .yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 0, color: .yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 0, color: .yellow)
        XCTAssertTrue(winChecker.isWinner(board: vc.board))
    }
    
    func testWinCheckerVerticalNegative() {
        let winChecker = WinChecker()
        
        vc.putCheckerAt(col: 0, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 0, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 0, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 0, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 0, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
    }
    
    func testWinCheckerDiagonalPositive() {
        let winChecker = WinChecker()
        
        vc.putCheckerAt(col: 0, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 1, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 1, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 2, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 2, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 2, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 3, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 3, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 3, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 3, color: Color.red)
        XCTAssertTrue(winChecker.isWinner(board: vc.board))
    }
    
    func testWinCheckerDiagonalPositive2() {
        let winChecker = WinChecker()
        
        vc.putCheckerAt(col: 5, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 4, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 4, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 3, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 3, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 3, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 2, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 2, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 2, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 2, color: Color.red)
        XCTAssertTrue(winChecker.isWinner(board: vc.board))
    }
    
    func testWinCheckerDiagonalNegative() {
        let winChecker = WinChecker()
        
        vc.putCheckerAt(col: 5, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 4, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 4, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 3, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 3, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 3, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 2, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 2, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 2, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 2, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 1, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 1, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 1, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 1, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 1, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        vc.printBoard()
    }
    
    func testWinCheckerDiagonalPositive3() {
        let winChecker = WinChecker()
        
        vc.putCheckerAt(col: 5, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 4, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 4, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 3, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 3, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 3, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 2, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 2, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 2, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 2, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 1, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 1, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 1, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 1, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 1, color: Color.red)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 0, color: Color.yellow)
        XCTAssertFalse(winChecker.isWinner(board: vc.board))
        
        vc.putCheckerAt(col: 3, color: Color.yellow)
        XCTAssertTrue(winChecker.isWinner(board: vc.board))
    }
    
    func testPossibleMoves1() {
        let possibleMovesChecker = PossibleMovesChecker()
        print(possibleMovesChecker.getPossibleMoves(board: vc.board).count)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 7)
        
        vc.putCheckerAt(col: 0, color: Color.yellow)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 7)
        vc.putCheckerAt(col: 0, color: Color.red)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 7)
        vc.putCheckerAt(col: 0, color: Color.yellow)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 7)
        vc.putCheckerAt(col: 0, color: Color.red)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 7)
        vc.putCheckerAt(col: 0, color: Color.yellow)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 7)
        vc.putCheckerAt(col: 0, color: Color.red)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 6)
    }
    
    func testPossibleMoves2() {
        let possibleMovesChecker = PossibleMovesChecker()
        print(possibleMovesChecker.getPossibleMoves(board: vc.board).count)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 7)
        
        vc.putCheckerAt(col: 0, color: Color.yellow)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 7)
        vc.putCheckerAt(col: 0, color: Color.red)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 7)
        vc.putCheckerAt(col: 0, color: Color.yellow)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 7)
        vc.putCheckerAt(col: 0, color: Color.red)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 7)
        vc.putCheckerAt(col: 0, color: Color.yellow)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 7)
        vc.putCheckerAt(col: 0, color: Color.red)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 6)
        
        vc.putCheckerAt(col: 1, color: Color.yellow)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 6)
        vc.putCheckerAt(col: 1, color: Color.red)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 6)
        vc.putCheckerAt(col: 1, color: Color.yellow)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 6)
        vc.putCheckerAt(col: 1, color: Color.red)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 6)
        vc.putCheckerAt(col: 1, color: Color.yellow)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 6)
        vc.putCheckerAt(col: 1, color: Color.red)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 5)
        
        vc.putCheckerAt(col: 2, color: Color.yellow)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 5)
        vc.putCheckerAt(col: 2, color: Color.red)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 5)
        vc.putCheckerAt(col: 2, color: Color.yellow)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 5)
        vc.putCheckerAt(col: 2, color: Color.red)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 5)
        vc.putCheckerAt(col: 2, color: Color.yellow)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 5)
        vc.putCheckerAt(col: 2, color: Color.red)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 4)
        
        vc.putCheckerAt(col: 3, color: Color.yellow)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 4)
        vc.putCheckerAt(col: 3, color: Color.red)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 4)
        vc.putCheckerAt(col: 3, color: Color.yellow)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 4)
        vc.putCheckerAt(col: 3, color: Color.red)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 4)
        vc.putCheckerAt(col: 3, color: Color.yellow)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 4)
        vc.putCheckerAt(col: 3, color: Color.red)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 3)
        
        vc.putCheckerAt(col: 4, color: Color.yellow)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 3)
        vc.putCheckerAt(col: 4, color: Color.red)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 3)
        vc.putCheckerAt(col: 4, color: Color.yellow)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 3)
        vc.putCheckerAt(col: 4, color: Color.red)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 3)
        vc.putCheckerAt(col: 4, color: Color.yellow)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 3)
        vc.putCheckerAt(col: 4, color: Color.red)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 2)
        
        vc.putCheckerAt(col: 5, color: Color.yellow)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 2)
        vc.putCheckerAt(col: 5, color: Color.red)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 2)
        vc.putCheckerAt(col: 5, color: Color.yellow)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 2)
        vc.putCheckerAt(col: 5, color: Color.red)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 2)
        vc.putCheckerAt(col: 5, color: Color.yellow)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 2)
        vc.putCheckerAt(col: 5, color: Color.red)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 1)
        
        vc.putCheckerAt(col: 6, color: Color.yellow)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 1)
        vc.putCheckerAt(col: 6, color: Color.red)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 1)
        vc.putCheckerAt(col: 6, color: Color.yellow)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 1)
        vc.putCheckerAt(col: 6, color: Color.red)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 1)
        vc.putCheckerAt(col: 6, color: Color.yellow)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 1)
        vc.putCheckerAt(col: 6, color: Color.red)
        XCTAssertTrue(possibleMovesChecker.getPossibleMoves(board: vc.board).count == 0)
    }
    
    func testAIPlayerCopyBoard() {
        let aiPlayer = AIPlayer()
        vc.putCheckerAt(col: 0, color: .red)
        vc.putCheckerAt(col: 1, color: .yellow)
        vc.putCheckerAt(col: 2, color: .red)
        vc.putCheckerAt(col: 3, color: .yellow)
        vc.putCheckerAt(col: 4, color: .red)
        vc.putCheckerAt(col: 5, color: .yellow)
        
        vc.putCheckerAt(col: 0, color: .red)
        vc.putCheckerAt(col: 1, color: .yellow)
        vc.putCheckerAt(col: 2, color: .red)
        vc.putCheckerAt(col: 3, color: .yellow)
        vc.putCheckerAt(col: 4, color: .red)
        vc.putCheckerAt(col: 5, color: .yellow)
        
        vc.putCheckerAt(col: 0, color: .red)
        vc.putCheckerAt(col: 1, color: .yellow)
        vc.putCheckerAt(col: 2, color: .red)
        vc.putCheckerAt(col: 3, color: .yellow)
        vc.putCheckerAt(col: 4, color: .red)
        vc.putCheckerAt(col: 5, color: .yellow)
        
        vc.putCheckerAt(col: 0, color: .red)
        vc.putCheckerAt(col: 1, color: .yellow)
        vc.putCheckerAt(col: 2, color: .red)
        vc.putCheckerAt(col: 3, color: .yellow)
        vc.putCheckerAt(col: 4, color: .red)
        vc.putCheckerAt(col: 5, color: .yellow)
        
        let copyBoard = aiPlayer.copyBoard(board: vc.board)
        for i in 0...vc.board.count-1 {
            for j in 0...vc.board[i].count-1 {
                XCTAssert(vc.board[i][j].color == copyBoard[i][j].color)
            }
        }
    }
    
    func testIsValidMove() {
        let inARowChecker = InARowChecker()
        vc.putCheckerAt(col: 0, color: .red)
        XCTAssertTrue(inARowChecker.isValidMove(board: vc.board, move: 0))
        vc.putCheckerAt(col: 0, color: .yellow)
        XCTAssertTrue(inARowChecker.isValidMove(board: vc.board, move: 0))
        vc.putCheckerAt(col: 0, color: .red)
        XCTAssertTrue(inARowChecker.isValidMove(board: vc.board, move: 0))
        vc.putCheckerAt(col: 0, color: .yellow)
        XCTAssertTrue(inARowChecker.isValidMove(board: vc.board, move: 0))
        vc.putCheckerAt(col: 0, color: .red)
        XCTAssertTrue(inARowChecker.isValidMove(board: vc.board, move: 0))
        vc.putCheckerAt(col: 0, color: .yellow)
        vc.printBoard()
        XCTAssertFalse(inARowChecker.isValidMove(board: vc.board, move: 0))
    }
    
    func testInARowCheckerHorizontal1() {
        let inARowChecker = InARowChecker()
        vc.putCheckerAt(col: 0, color: .red)
        XCTAssertTrue(inARowChecker.checkHorizontal(color: .red, board: vc.board, num: 3) == 0)
        vc.putCheckerAt(col: 1, color: .red)
        XCTAssertTrue(inARowChecker.checkHorizontal(color: .red, board: vc.board, num: 3) == 0)
        vc.putCheckerAt(col: 2, color: .red)
        XCTAssertTrue(inARowChecker.checkHorizontal(color: .red, board: vc.board, num: 3) == 3)
        vc.putCheckerAt(col: 3, color: .yellow)
        XCTAssertTrue(inARowChecker.checkHorizontal(color: .red, board: vc.board, num: 3) == 0)
        vc.putCheckerAt(col: 4, color: .red)
        XCTAssertTrue(inARowChecker.checkHorizontal(color: .red, board: vc.board, num: 3) == 0)
        vc.putCheckerAt(col: 5, color: .yellow)
        XCTAssertTrue(inARowChecker.checkHorizontal(color: .red, board: vc.board, num: 3) == 0)
    }
    
    func testInARowCheckerHorizontal2() {
        let inARowChecker = InARowChecker()
        vc.putCheckerAt(col: 1, color: .red)
        XCTAssertTrue(inARowChecker.checkHorizontal(color: .red, board: vc.board, num: 3) == 0)
        vc.putCheckerAt(col: 2, color: .red)
        XCTAssertTrue(inARowChecker.checkHorizontal(color: .red, board: vc.board, num: 3) == 0)
        vc.putCheckerAt(col: 3, color: .red)
        XCTAssertTrue(inARowChecker.checkHorizontal(color: .red, board: vc.board, num: 3) == 30)
    }
    
    func testInARowCheckerVertical1() {
        let inARowChecker = InARowChecker()
        vc.putCheckerAt(col: 1, color: .red)
        XCTAssertTrue(inARowChecker.checkVertical(color: .red, board: vc.board, num: 3) == 0)
        vc.putCheckerAt(col: 1, color: .red)
        XCTAssertTrue(inARowChecker.checkVertical(color: .red, board: vc.board, num: 3) == 0)
        vc.putCheckerAt(col: 1, color: .red)
        XCTAssertTrue(inARowChecker.checkVertical(color: .red, board: vc.board, num: 3) == 3)
        vc.putCheckerAt(col: 1, color: .yellow)
        XCTAssertTrue(inARowChecker.checkVertical(color: .red, board: vc.board, num: 3) == 0)
    }
    
    func testInARowCheckerVertical2() {
        let inARowChecker = InARowChecker()
        vc.putCheckerAt(col: 1, color: .yellow)
        XCTAssertTrue(inARowChecker.checkVertical(color: .red, board: vc.board, num: 3) == 0)
        vc.putCheckerAt(col: 1, color: .red)
        XCTAssertTrue(inARowChecker.checkVertical(color: .red, board: vc.board, num: 3) == 0)
        vc.putCheckerAt(col: 1, color: .red)
        XCTAssertTrue(inARowChecker.checkVertical(color: .red, board: vc.board, num: 3) == 0)
        vc.putCheckerAt(col: 1, color: .red)
        XCTAssertTrue(inARowChecker.checkVertical(color: .red, board: vc.board, num: 3) == 3)
        vc.putCheckerAt(col: 1, color: .yellow)
        XCTAssertTrue(inARowChecker.checkVertical(color: .red, board: vc.board, num: 3) == 0)
    }
    
    func testInRowCheckerDiagonal1() {
        let inARowChecker = InARowChecker()
        vc.putCheckerAt(col: 1, color: .red)
        XCTAssertTrue(inARowChecker.checkDiagonal(color: .red, board: vc.board, num: 3) == 0)
        vc.putCheckerAt(col: 2, color: .yellow)
        XCTAssertTrue(inARowChecker.checkDiagonal(color: .red, board: vc.board, num: 3) == 0)
        vc.putCheckerAt(col: 2, color: .red)
        XCTAssertTrue(inARowChecker.checkDiagonal(color: .red, board: vc.board, num: 3) == 0)
        vc.putCheckerAt(col: 3, color: .yellow)
        XCTAssertTrue(inARowChecker.checkDiagonal(color: .red, board: vc.board, num: 3) == 0)
        vc.putCheckerAt(col: 3, color: .yellow)
        XCTAssertTrue(inARowChecker.checkDiagonal(color: .red, board: vc.board, num: 3) == 0)
        vc.putCheckerAt(col: 3, color: .red)
        vc.printBoard()
        XCTAssertTrue(inARowChecker.checkDiagonal(color: .red, board: vc.board, num: 3) == 3)
    }
    
    func testInRowCheckerDiagonal2() {
        let inARowChecker = InARowChecker()
        vc.putCheckerAt(col: 1, color: .yellow)
        XCTAssertTrue(inARowChecker.checkDiagonal(color: .red, board: vc.board, num: 3) == 0)
        vc.putCheckerAt(col: 1, color: .red)
        XCTAssertTrue(inARowChecker.checkDiagonal(color: .red, board: vc.board, num: 3) == 0)
        vc.putCheckerAt(col: 2, color: .yellow)
        XCTAssertTrue(inARowChecker.checkDiagonal(color: .red, board: vc.board, num: 3) == 0)
        vc.putCheckerAt(col: 2, color: .yellow)
        XCTAssertTrue(inARowChecker.checkDiagonal(color: .red, board: vc.board, num: 3) == 0)
        vc.putCheckerAt(col: 2, color: .red)
        XCTAssertTrue(inARowChecker.checkDiagonal(color: .red, board: vc.board, num: 3) == 0)
        vc.putCheckerAt(col: 3, color: .yellow)
        XCTAssertTrue(inARowChecker.checkDiagonal(color: .red, board: vc.board, num: 3) == 0)
        vc.putCheckerAt(col: 3, color: .yellow)
        XCTAssertTrue(inARowChecker.checkDiagonal(color: .red, board: vc.board, num: 3) == 0)
        vc.putCheckerAt(col: 3, color: .yellow)
        XCTAssertTrue(inARowChecker.checkDiagonal(color: .red, board: vc.board, num: 3) == 0)
        vc.putCheckerAt(col: 3, color: .red)
        vc.printBoard()
    }
    
}
